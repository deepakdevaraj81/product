package com.mytoys.product.controller;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.mytoys.product.model.ProductVO;
import com.mytoys.product.service.ProductService;

@RunWith(MockitoJUnitRunner.class)
public class ProductControllerTest {

	@Mock
	ProductService productService;
	
	@Test
	public void getProductsTest() throws Exception {
		List<ProductVO> productVOList = new ArrayList<ProductVO>();
		ProductVO productVO = new ProductVO();
		productVOList.add(productVO);
		Mockito.when(productService.getProducts()).thenReturn(productVOList);
	}
	
	@Test
	public void getProductByIdTest() throws Exception {
		List<ProductVO> productVOList = new ArrayList<ProductVO>();
		ProductVO productVO = new ProductVO();
		productVOList.add(productVO);
		Mockito.when(productService.getProductById(Mockito.anyLong())).thenReturn(productVOList);
	}
	
	@Test
	public void addProductTest() throws Exception {
		ProductVO productVO = new ProductVO();
		productVO.setProductId(778899L);
		productVO.setProductName("Test");
		productVO.setPrice(10d);
		productVO.setOldPrice(8d);
		productVO.setStockCount(12);
		Mockito.doCallRealMethod().when(productService).addProduct(productVO,null);
	}
	
	@Test
	public void updateProductTest() throws Exception {
		ProductVO productVO = new ProductVO();
		productVO.setProductId(778899L);
		productVO.setProductName("Test");
		productVO.setPrice(10d);
		productVO.setOldPrice(8d);
		productVO.setStockCount(12);
		Mockito.doCallRealMethod().when(productService).addProduct(productVO,778899L);
	}
}
