package com.mytoys.product.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.mytoys.product.entity.Brand;
import com.mytoys.product.entity.Price;
import com.mytoys.product.entity.Product;
import com.mytoys.product.entity.Stock;
import com.mytoys.product.repository.BrandRepository;
import com.mytoys.product.repository.PriceRepository;
import com.mytoys.product.repository.ProductRepository;
import com.mytoys.product.repository.StockRepository;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceTest {

	@Mock
	ProductRepository productRepository;
	
	@Mock
	PriceRepository priceRepository;
	
	@Mock
	BrandRepository brandRepository;
	
	@Mock
	StockRepository stockRepository;
	
	@Test
	public void getProductsTest() {
		
		
		List<Product> productList = new ArrayList<Product>();
		Product product = new Product();
		product.setProductId(47271L);
		product.setProductName("PLAYMOBIL #3755351");
		productList.add(product);
		Mockito.when(productRepository.findAll()).thenReturn(productList);
		Price price = new Price();
		price.setProductId(47271L);
		price.setNewPrice(28.99);
		price.setOldPrice(37.99);
		
		Brand brand = new Brand();
		brand.setBrandName("PLAYMOBIL");
		
		Stock stock = new Stock();
		stock.setProductId(47271L);
		stock.setStockCount(114);
		
		productList.forEach(prod -> {
			Mockito.when(priceRepository.findByProductId(Mockito.eq(47271L))).thenReturn(price);
			
			Mockito.when(stockRepository.findByProductId(Mockito.eq(47271L))).thenReturn(stock);
			
		});
		
	}
	
	@Test
	public void getProductByIdTest() {
		Product product = new Product();
		product.setProductId(47271L);
		product.setProductName("PLAYMOBIL #3755351");
		Mockito.when(productRepository.findByProductId(Mockito.eq(47271L))).thenReturn(product);
	}
	
	@Test
	public void addProductTest() {
		Product product = new Product();
		product.setProductId(778899L);
		product.setProductName("Test");
		//Mockito.doCallRealMethod().when(productRepository).save(product);
	}
}
