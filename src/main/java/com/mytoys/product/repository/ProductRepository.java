package com.mytoys.product.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mytoys.product.entity.Product;

/**
 * @author Deepak Devaraj
 * This is a ProductRepository class which is the Repository layer of the Products API.
 */

@Repository
public interface ProductRepository extends JpaRepository<Product , Long> {

	public List<Product> findAll();
	public Optional<Product> findById(@Param("id") Long id);
	public Product findByProductId(@Param("productId") Long productId);
}
