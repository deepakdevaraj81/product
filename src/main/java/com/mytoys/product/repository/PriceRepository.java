package com.mytoys.product.repository;

import java.util.List;
import java.util.Optional;

import com.mytoys.product.entity.Price;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

/**
 * @author Deepak Devaraj
 * This is a PriceRepository class which is the Repository layer of the Products API.
 */
public interface PriceRepository extends JpaRepository<Price , Long> {

	public List<Price> findAll();
	public Optional<Price> findById(@Param("id") Long id);
	public Price findByProductId(@Param("productId") Long productId);
}
