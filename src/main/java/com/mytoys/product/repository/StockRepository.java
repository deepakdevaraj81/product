package com.mytoys.product.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.mytoys.product.entity.Stock;

/**
 * @author Deepak Devaraj
 * This is a StockRepository class which is the Repository layer of the Products API.
 */
public interface StockRepository extends JpaRepository<Stock , Long> {

	public List<Stock> findAll();
	public Optional<Stock> findById(@Param("id") Long id);
	public Stock findByProductId(@Param("productId") Long productId);
}
