package com.mytoys.product.repository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.mytoys.product.entity.Brand;

/**
 * @author Deepak Devaraj
 * This is a BrandRepository class which is the Repository layer of the Products API.
 */
public interface BrandRepository extends JpaRepository<Brand , Long> {

	public List<Brand> findAll();
	public Brand findByBrandId(@Param("brandId") Long brandId);
	public Brand findByBrandName(@Param("brandName") String brandName);
}
