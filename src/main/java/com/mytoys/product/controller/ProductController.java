package com.mytoys.product.controller;

import java.io.FileNotFoundException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mytoys.product.exception.ProductCreateException;
import com.mytoys.product.exception.ProductNotFoundException;
import com.mytoys.product.exception.ProductsFetchException;
import com.mytoys.product.exception.ProductsLoadException;
import com.mytoys.product.model.ProductVO;
import com.mytoys.product.service.ProductService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author Deepak Devaraj
 * This is a ProductController class which is the Controller layer that is the entry point of the Products API.
 */

@Controller
@RequestMapping(value="/product")
@Api(tags = { "Products API" })
public class ProductController {
	
	@Autowired
	ProductService productService;
	
	/**
     * Method that gets the list of Products from the DB
     * @return ResponseEntity<List<ProductVO>>.
     * @throws exception of ProductsFetchException , Exception type.
     */
	
	@RequestMapping(method=RequestMethod.GET , produces="application/json"  )
	@ApiOperation(value = "Get List of Products", notes = "This API fetches all the list of Products.")
	public ResponseEntity<List<ProductVO>> getProducts() throws ProductsFetchException , Exception {
		return new ResponseEntity<List<ProductVO>>(productService.getProducts(), HttpStatus.OK);
	}
	
	/**
     * Method that gets the Product by ProductId from the DB
     * @param Long productId
     * @return ResponseEntity<List<ProductVO>>.
     * @throws exception of ProductsFetchException , Exception type.
     */
	@RequestMapping(method=RequestMethod.GET , value="/{productId:[\\d]+}")
	@ApiOperation(value = "Get Product By Id", notes = "This API fetches Product By Id.")
	public ResponseEntity<List<ProductVO>> getProductById(@PathVariable Long productId) throws ProductNotFoundException , Exception  {
		return new ResponseEntity<List<ProductVO>>(productService.getProductById(productId), HttpStatus.OK);
	}
	
	/**
     * Method that reads an Excel file and loads the Products to the DB.
     * @param Long productId
     * @return ResponseEntity<List<ProductVO>>.
     * @throws exception of ProductsFetchException , Exception type.
     */
	@RequestMapping(method=RequestMethod.POST , value="/loadProducts")
	@ApiOperation(value = "Load Products from Excel", notes = "This API reads an Excel file and loads the Products to the DB.")
	public ResponseEntity<HttpStatus> loadProducts() throws ProductsLoadException , FileNotFoundException , Exception  {
		productService.loadProducts();
		return new ResponseEntity<HttpStatus>(HttpStatus.OK);
	}
	
	/**
     * Method that adds a new Product to the DB.
     * @param ProductVO productVO
     * @return ResponseEntity<HttpStatus>.
     * @throws exception of ProductsFetchException , Exception type.
     */
	@RequestMapping(method=RequestMethod.POST , consumes="application/json" )
	@ApiOperation(value = "Add a new Product", notes = "This API adds a new Product to the DB.")
	public ResponseEntity<HttpStatus> addProduct(@RequestBody ProductVO productVO) throws ProductCreateException , Exception  {
		productService.addProduct(productVO , null);
		return new ResponseEntity<HttpStatus>(HttpStatus.OK);
	}
	
	/**
     * Method that updates an existing Product to the DB.
     * @param ProductVO productVO , Long productId
     * @return ResponseEntity<HttpStatus>.
     * @throws exception of ProductsFetchException , Exception type.
     */
	@RequestMapping(method=RequestMethod.PUT , value="/{productId:[\\d]+}" , consumes="application/json")
	@ApiOperation(value = "Update Product", notes = "This API updates an existing product with given productId to the DB.")
	public ResponseEntity<HttpStatus> updateProduct(@RequestBody ProductVO productVO , @PathVariable("productId") Long productId ) throws ProductCreateException , Exception  {
		productService.addProduct(productVO , productId);
		return new ResponseEntity<HttpStatus>(HttpStatus.OK);
	}

}
