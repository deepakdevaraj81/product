package com.mytoys.product.service;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.mytoys.product.entity.Brand;
import com.mytoys.product.entity.Price;
import com.mytoys.product.entity.Product;
import com.mytoys.product.entity.Stock;
import com.mytoys.product.exception.ProductCreateException;
import com.mytoys.product.exception.ProductNotFoundException;
import com.mytoys.product.exception.ProductsFetchException;
import com.mytoys.product.exception.ProductsLoadException;
import com.mytoys.product.model.ProductVO;
import com.mytoys.product.repository.BrandRepository;
import com.mytoys.product.repository.PriceRepository;
import com.mytoys.product.repository.ProductRepository;
import com.mytoys.product.repository.StockRepository;

/**
 * @author Deepak Devaraj
 * This is a ProductService class which is a Service layer that does the business logic.
 */

@Service
@PropertySource("classpath:application.properties")
public class ProductService {
	
	@Autowired
	ProductRepository productRepository;
	
	@Autowired
	PriceRepository priceRepository;
	
	@Autowired
	BrandRepository brandRepository;
	
	@Autowired
	StockRepository stockRepository;
	
	@Value("${PRODUCTS_FILE_PATH}")
    private String productsFilePath;
	
	/**
     * Method that gets the list of Products from the DB
     * @return List<ProductVO>.
     * @throws exception of ProductsFetchException , Exception type.
     */
	public List<ProductVO> getProducts() throws ProductsFetchException , Exception {
		List<Product> productList = productRepository.findAll();
		List<ProductVO> productVOList = new ArrayList<ProductVO>();
		productList.forEach(product -> {
			ProductVO productVO = new ProductVO();
			productVO.setProductId(product.getProductId());
			productVO.setProductName(product.getProductName());
			
			Price price = priceRepository.findByProductId(product.getProductId());
			productVO.setPrice(price.getNewPrice());
			productVO.setOldPrice(price.getOldPrice());
			
			Optional<Brand> brand = brandRepository.findById(product.getBrandId());
			productVO.setBrand(brand.get().getBrandName());
			
			Stock stock = stockRepository.findByProductId(product.getProductId());
			productVO.setStockCount(stock.getStockCount());
			
			productVOList.add(productVO);
			
		}
		
		);
		return productVOList;
	}
	
	/**
     * Method that gets the Product by ProductId
     * @param Long productId
     * @return List<ProductVO>.
     * @throws exception of ProductNotFoundException , Exception type.
     */
	public List<ProductVO> getProductById(Long productId) throws ProductNotFoundException , Exception  {
		Product product = productRepository.findByProductId(productId);
		List<ProductVO> productVOList = new ArrayList<ProductVO>();
		
		ProductVO productVO = new ProductVO();
		productVO.setProductId(product.getProductId());
		productVO.setProductName(product.getProductName());
			
		Price price = priceRepository.findByProductId(product.getProductId());
		productVO.setPrice(price.getNewPrice());
		productVO.setOldPrice(price.getOldPrice());
			
		Optional<Brand> brand = brandRepository.findById(product.getBrandId());
		productVO.setBrand(brand.get().getBrandName());
			
		Stock stock = stockRepository.findByProductId(product.getProductId());
		productVO.setStockCount(stock.getStockCount());
			
		productVOList.add(productVO);
			
		
		return productVOList;
	}
	
	/**
     * Method that reads and loads Products from the Excel Sheet to the DB
     * @return void
     * @throws exception of ProductsLoadException , Exception type.
     */
	public void loadProducts() throws ProductsLoadException , ProductCreateException , FileNotFoundException , Exception {
		
		List<ProductVO> productVOList = null;
		try {
			productVOList = new ArrayList<ProductVO>();
			
			InputStream input = getClass().getResourceAsStream(productsFilePath);
			
			Workbook workbook = new XSSFWorkbook(input);
	        Sheet datatypeSheet = workbook.getSheetAt(0);
	        Iterator<Row> iterator = datatypeSheet.iterator();
	
	        while (iterator.hasNext()) {
	
	            Row currentRow = iterator.next();
	            Iterator<Cell> cellIterator = currentRow.iterator();
	
	            ProductVO productVO = new ProductVO();
	            while (cellIterator.hasNext()) {
	
	                Cell currentCell = cellIterator.next();
	                
	                if(currentCell.getRowIndex()!=0) {
		                if(currentCell.getColumnIndex()==0) {
		                	if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
		                		productVO.setProductId(Long.valueOf(Double.valueOf(currentCell.getNumericCellValue()).longValue()));
		                	}	
		                } else if(currentCell.getColumnIndex()==1) {
		                	if (currentCell.getCellTypeEnum() == CellType.STRING) {
		                		productVO.setProductName(currentCell.getStringCellValue());
		                	}	
		                } else if(currentCell.getColumnIndex()==2) {
		                	if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
		                		productVO.setPrice(Double.valueOf(currentCell.getNumericCellValue()));
		                	}	
		                } else if(currentCell.getColumnIndex()==3) {
		                	if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
		                		productVO.setOldPrice(Double.valueOf(currentCell.getNumericCellValue()));
		                	}	
		                } else if(currentCell.getColumnIndex()==4) {
		                	if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
		                		productVO.setStockCount(Integer.valueOf(Double.valueOf(currentCell.getNumericCellValue()).intValue()));
		                	}	
		                } else if(currentCell.getColumnIndex()==5) {
		                	if (currentCell.getCellTypeEnum() == CellType.STRING) {
		                		productVO.setBrand(currentCell.getStringCellValue());
		                	}	
		                }
	                
	                }
	            }
	            productVOList.add(productVO);
	
	        }
	        
	        productVOList.stream().filter(product->product.getProductId()!=null).forEach(product -> {
	        	try {
	        		addProduct(product , null);
	        	} catch(Exception e) {
	        		e.printStackTrace();
	        	}
	        	
	        });
		} catch(FileNotFoundException fe) {
			fe.printStackTrace();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	/**
     * Method that adds a new Product to the DB
     * @return void
     * @param ProductVO productVO
     * @throws exception of ProductCreateException , Exception type.
     */
	public void addProduct(ProductVO productVO , Long productId) throws ProductCreateException , Exception {
		Product productEntity = null;
		if(productId==null) {
			productEntity = productRepository.findByProductId(productVO.getProductId());
			if(productEntity!=null) {
				throw new ProductCreateException("Duplicate Product Id "+productVO.getProductId());
			} else {
				productEntity = new Product();
				productEntity.setProductId(productVO.getProductId());
				productEntity.setProductName(productVO.getProductName());
			}	
		} else {
			productEntity = productRepository.findByProductId(productId);
			productEntity.setProductName(productVO.getProductName());
		}
    	Brand brand = brandRepository.findByBrandName(productVO.getBrand());
    	if(brand==null) {
    		brand = new Brand();
    		brand.setBrandName(productVO.getBrand());
    	} 
    	brandRepository.save(brand);
    	
    	productEntity.setBrandId(brand.getBrandId());
    	Product productPersistedEntity = productRepository.save(productEntity);
    	
    	Price price = priceRepository.findByProductId(productVO.getProductId());
    	if(price==null) {
    		price = new Price();
    		price.setProductId(productVO.getProductId());
    		price.setNewPrice(productVO.getPrice());
    		price.setOldPrice(productVO.getOldPrice());
    		
    	} else {
    		price.setNewPrice(productVO.getPrice());
    		price.setOldPrice(productVO.getOldPrice());
    	}
    	priceRepository.save(price);
    	
    	Stock stock = stockRepository.findByProductId(productVO.getProductId());
    	if(stock==null) {
    		stock = new Stock();
    		stock.setProductId(productVO.getProductId());
    		stock.setStockCount(productVO.getStockCount());
    		
    	} else {
    		stock.setStockCount(productVO.getStockCount());
    	}
    	stockRepository.save(stock);
	}

}
