package com.mytoys.product.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.io.FileNotFoundException;
import java.time.LocalDateTime;

/**
 * @author Deepak Devaraj
 * This is an Exception Handler class which handles the Exceptions thrown by the application.
 */
@ControllerAdvice
public class CustomGlobalExceptionHandler extends ResponseEntityExceptionHandler {

	/**
     * Method that handles ProductsLoadException
     * @param ex of Exception type.
     * @param request of WebRequest type.
     * @return new object of ResponseEntity<CustomErrorResponse> type.
     */
    @ExceptionHandler(ProductsLoadException.class)
    public ResponseEntity<CustomErrorResponse> customHandleNotFound(Exception ex, WebRequest request) {

        CustomErrorResponse errors = new CustomErrorResponse();
        errors.setTimestamp(LocalDateTime.now());
        errors.setError(ex.getMessage());
        errors.setStatus(HttpStatus.NOT_FOUND.value());
        return new ResponseEntity<>(errors, HttpStatus.NOT_FOUND);
    }
    
    /**
     * Method that handles ProductCreateException
     * @param ex of Exception type.
     * @param request of WebRequest type.
     * @return new object of ResponseEntity<CustomErrorResponse> type.
     */
    @ExceptionHandler(ProductCreateException.class)
    public ResponseEntity<CustomErrorResponse> customHandleCreate(Exception ex, WebRequest request) {

        CustomErrorResponse errors = new CustomErrorResponse();
        errors.setTimestamp(LocalDateTime.now());
        errors.setError(ex.getMessage());
        errors.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        return new ResponseEntity<>(errors, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    
    /**
     * Method that handles ProductNotFoundException
     * @param ex of Exception type.
     * @param request of WebRequest type.
     * @return new object of ResponseEntity<CustomErrorResponse> type.
     */
    @ExceptionHandler(ProductNotFoundException.class)
    public ResponseEntity<CustomErrorResponse> customHandleProductNotFound(Exception ex, WebRequest request) {

        CustomErrorResponse errors = new CustomErrorResponse();
        errors.setTimestamp(LocalDateTime.now());
        errors.setError(ex.getMessage());
        errors.setStatus(HttpStatus.NOT_FOUND.value());
        return new ResponseEntity<>(errors, HttpStatus.NOT_FOUND);
    }
    
    /**
     * Method that handles ProductsFetchException
     * @param ex of Exception type.
     * @param request of WebRequest type.
     * @return new object of ResponseEntity<CustomErrorResponse> type.
     */
    @ExceptionHandler(ProductsFetchException.class)
    public ResponseEntity<CustomErrorResponse> customHandleProductFetchException(Exception ex, WebRequest request) {

        CustomErrorResponse errors = new CustomErrorResponse();
        errors.setTimestamp(LocalDateTime.now());
        errors.setError(ex.getMessage());
        errors.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        return new ResponseEntity<>(errors, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    
    
    /**
     * Method that handles FileNotFoundException
     * @param ex of Exception type.
     * @param request of WebRequest type.
     * @return new object of ResponseEntity<CustomErrorResponse> type.
     */
    @ExceptionHandler(FileNotFoundException.class)
    public ResponseEntity<CustomErrorResponse> customHandleFileNotFound(Exception ex, WebRequest request) {

        CustomErrorResponse errors = new CustomErrorResponse();
        errors.setTimestamp(LocalDateTime.now());
        errors.setError(ex.getMessage());
        errors.setStatus(HttpStatus.NOT_FOUND.value());
        return new ResponseEntity<>(errors, HttpStatus.NOT_FOUND);
    }
    
}
