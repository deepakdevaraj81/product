package com.mytoys.product.exception;

/**
 * @author Deepak Devaraj
 * This is a ProductNotFoundException class which is a Custom User Exception of the Products API.
 */
public class ProductNotFoundException extends Exception {
	
	public ProductNotFoundException(Long productId) {
        super("Product Not Found Exception : " + productId);
    }
	public ProductNotFoundException(String status) {
        super("Product Not Found Exception : " + status);
    }

}
