package com.mytoys.product.exception;

/**
 * @author Deepak Devaraj
 * This is a ProductsLoadException class which is a Custom User Exception of the Products API.
 */
public class ProductsLoadException extends Exception {
	
	public ProductsLoadException(Long productId) {
        super("Products Load Exception : " + productId);
    }
	public ProductsLoadException(String status) {
        super("Products Load Exception : " + status);
    }

}
