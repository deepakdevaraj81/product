package com.mytoys.product.exception;

/**
 * @author Deepak Devaraj
 * This is a ProductCreateException class which is a Custom User Exception of the Products API.
 */
public class ProductCreateException extends Exception {
	
	public ProductCreateException(Long productId) {
        super("Product Create Exception : " + productId);
    }
	public ProductCreateException(String status) {
        super("Product Create Exception : " + status);
    }

}
