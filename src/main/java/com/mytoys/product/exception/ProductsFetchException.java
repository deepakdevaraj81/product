package com.mytoys.product.exception;

/**
 * @author Deepak Devaraj
 * This is a ProductsFetchException class which is a Custom User Exception of the Products API.
 */
public class ProductsFetchException extends Exception {
	
	public ProductsFetchException(Long productId) {
        super("Unable to fetch Exception : " + productId);
    }
	public ProductsFetchException(String status) {
        super("Unable to fetch Exception : " + status);
    }

}
